# Portuguese corpus tokenizer 

import os
from pathlib import Path
from tokenizers.implementations import ByteLevelBPETokenizer

# Getting the necessary paths
paths = [str(x) for x in Path("./data_src/").glob("**/*.txt")]

# Initializing a tokenizer
tokenizer = ByteLevelBPETokenizer()

# Customizing our training and using RoBERTa's special tokens
# We arbitrarily pick the size to be 50.000
tokenizer.train(files=paths, vocab_size=125_000, min_frequency=2, special_tokens=[
    "<s>",
    "<pad>",
    "</s>",
    "<unk>",
    "<mask>"
])

# Saving the files to disk
path = "./portubert/models/PortuBERT-small"
os.makedirs(path, exist_ok=True)
tokenizer.save_model(path)