<div align="center">
    <img src="src/img/logo.png" width="350" height="200">
</div>

<br>

PortuBERT, or more technically **PortuBERT-small**, is a Portuguese-based language model with roughly 182 million parameters. It possesses traits from DistilBERT, RoBERTa and GPT-2. The goal of this project is twofold:

* Stimulate the research, usage and creation of new Portuguese language models;

* Explore the overall capacity of the transformer architecture to adapt to the Portuguese language. <br><br>

**Disclaimer:** Please note that the size and complexity of the language model, and the fact that I'm an independent developer, have made training it on my own impossible. This page will be updated as soon as I manage to secure a way to have PortuBERT trained. Thus, for the time being, using the model without training it first is inadvisable. <br><br>

#### **Acknowledgements**

> The dataset that was used to build the corpus and train the model (roughly 3.2 GB) was acquired from the University of Leipzing's [Wortschatz](https://wortschatz.uni-leipzig.de/en) project, which contains a collection of corpora of several different languages in text format.

> The structure of this project was derived from Huggingface's several tutorials and articles on language models, specifically those based on the RoBERTa architecture.