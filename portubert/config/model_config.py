# Model configuration file

from transformers import RobertaConfig, RobertaTokenizerFast, RobertaForMaskedLM

# Given it's based on RoBERTa, we can use RobertaConfig to speed things up
# One major change is the vocab_size (75k vs. roughly 52k)
config = RobertaConfig(
    vocab_size=125000,
    max_position_embeddings=514,
    num_attention_heads=24,
    num_hidden_layers=12,
    type_vocab_size=1
)

# Loading our tokenizer and model
tokenizer = RobertaTokenizerFast.from_pretrained("./portubert/models/PortuBERT-small/", max_len=512)
model = RobertaForMaskedLM(config=config)