# Configuring the model's tokenizers 

from tokenizers.implementations import ByteLevelBPETokenizer
from tokenizers.processors import BertProcessing

# Loading the vocab and merges files
tokenizer = ByteLevelBPETokenizer(
    "./portubert/models/PortuBERT-small/vocab.json",
    "./portubert/models/PortuBERT-small/merges.txt",
)

# Using BertProcessing to convert tokens to IDs
tokenizer._tokenizer.post_processor = BertProcessing(
    ("</s>", tokenizer.token_to_id("</s>")),
    ("<s>", tokenizer.token_to_id("<s>")),
)

# Enabling truncation (if necessary)
tokenizer.enable_truncation(max_length=512)