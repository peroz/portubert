# PortuBERT's training definitions

from transformers import Trainer, TrainingArguments
from process_dataset import dataset, data_collator
from model_config import model

# Defining our training arguments
training_args = TrainingArguments(
    output_dir="./portubert/models/PortuBERT-small/",
    overwrite_output_dir=True,
    num_train_epochs=1,
    per_device_train_batch_size=64,
    save_steps=10_000,
    save_total_limit=2,
    prediction_loss_only=True,
)

# Defining our trainer and its arguments
trainer = Trainer(
    model=model,
    args=training_args,
    data_collator=data_collator,
    train_dataset=dataset,
)

# Finally, training our model!
trainer.train()

# Saving the trained model
trainer.save_model("./PortuBERT-small")