# Processing the training dataset
# NOTE: LineByLineDataset will be removed soon, and replaced with Huggingface Datasets

from transformers import DataCollatorForLanguageModeling
from model_config import tokenizer
from transformers import LineByLineTextDataset

# Reading our training dataset and applying our tokenizer
dataset = LineByLineTextDataset(
    tokenizer=tokenizer,
    file_path="./training/pt_train_data.txt",
    block_size=128
)

# Creating a data collator to help us with batches in PyTorch
data_collator = DataCollatorForLanguageModeling(
    tokenizer=tokenizer, mlm=True, mlm_probability=0.15
)
